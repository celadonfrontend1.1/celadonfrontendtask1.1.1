import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "../Header/header.css";

export const Header = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand id="navbar-brand" href="/">
            Tic Tac Toe
          </Navbar.Brand>
          <Nav className="justify-content-end">
            <Nav.Link className=" nav-links" href="/">
              Game
            </Nav.Link>
            <Nav.Link className=" nav-links" href="leaderboard">
              Leaderboard
            </Nav.Link>
            <Nav.Link className=" nav-links" href="rules">
              Rules
            </Nav.Link>
            <Nav.Link className=" nav-links" href="login">
              Log Out
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
