import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAXgTXkZRCTXojE3pBs44XaJpliNdoegbE",
  authDomain: "tictactoe-7e1d1.firebaseapp.com",
  projectId: "tictactoe-7e1d1",
  storageBucket: "tictactoe-7e1d1.appspot.com",
  messagingSenderId: "224615347175",
  appId: "1:224615347175:web:d9197234d4637b74b28342",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
