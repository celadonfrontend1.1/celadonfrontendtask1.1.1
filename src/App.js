import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./styles.css";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import Leaderboard from "./Pages/Leaderboard";
import Rules from "./Pages/Rules";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<Home />} />
          <Route path="/leaderboard" element={<Leaderboard />} />
          <Route path="/rules" element={<Rules />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
