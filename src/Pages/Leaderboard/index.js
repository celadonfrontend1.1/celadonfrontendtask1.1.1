import React from "react";
import "./styles.css";
import Table from "react-bootstrap/Table";
import Header from "../../Components/Header";
export const Leaderboard = () => {
  const profiles = JSON.parse(localStorage.getItem("profiles"));
  return (
    <>
      {" "}
      <Header />
      <div className="d-flex align-items-center justify-content-center vh-100">
        <Table className="w-75 table-dark" striped bordered>
          <thead>
            <tr>
              <th>Players</th>
              <th>Wins</th>
              <th>Lose</th>
              <th>Total Games</th>
              <th>Score</th>
              <th>Win Rate </th>
            </tr>
          </thead>
          <tbody>
            {profiles &&
              profiles.map((profile, index) => (
                <tr key={index}>
                  <td>{profile.username}</td>
                  <td>{profile.win}</td>
                  <td>{profile.lose}</td>
                  <td>{profile.totalGame}</td>
                  <td>{profile.score}</td>
                  <td>
                    {profile.totalGame > 0
                      ? (profile.win / profile.totalGame).toFixed(2)
                      : 0}
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default Leaderboard;
