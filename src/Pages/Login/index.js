import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { RecaptchaVerifier, signInWithPhoneNumber } from "firebase/auth";
import { auth } from "../../firebase.config";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import "../Login/styles.css";

export const Login = () => {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [confirmationResult, setConfirmationResult] = useState(null);
  const [otp, setOtp] = useState("");
  const navigation = useNavigate();
  const sendOtp = async () => {
    try {
      if (!phoneNumber) return alert("Please enter phone number");
      const recaptcha = new RecaptchaVerifier(auth, "recaptcha-container", {
        size: "invisible",
      });
      recaptcha.render();
      const confirmation = await signInWithPhoneNumber(
        auth,
        phoneNumber,
        recaptcha
      );
      setConfirmationResult(confirmation);
      console.log("Phone Number: ", phoneNumber);
    } catch (err) {
      console.log(err);
    }
  };

  const verifyClicked = async (e) => {
    var inputElements = document.querySelectorAll('input[type="text"]');
    var otp = "";

    inputElements.forEach(function (inputElement) {
      var value = inputElement.value || "";
      otp += value;
      setOtp(otp);
      console.log("OTP: ", otp);
    });
    e.preventDefault();
    console.log(confirmationResult, "confirmationResult");
    if (!otp) return alert("Please enter OTP");
    try {
      const result = await confirmationResult.confirm(otp);
      console.log(result);
      alert("OTP verified successfully");
    } catch (err) {
      console.log(err);
    }
    console.log(otp, "NEBU");
  };
  return (
    <>
      <div className="container">
        <div className="row justify-content-center align-items-center vh-100">
          <div className="col-4 text-center">
            <h1 className="font-weight-bold">Login</h1>
            <div className="mb-3">
              <input
                className="form-control"
                placeholder="Phone Number"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
            </div>

            <div>
              <Button
                id="login-button"
                className="btn btn-dark"
                onClick={sendOtp}
              >
                Login
              </Button>
              <div id="recaptcha-container"></div>
            </div>
          </div>

          <div className="container d-flex justify-content-center align-items-center">
            <div className=" text-center">
              <div className="input-container d-flex flex-row justify-content-center mt-2">
                <input
                  type="text"
                  className="m-1 text-center form-control rounded"
                  maxLength="1"
                />
                <input
                  type="text"
                  className="m-1 text-center form-control rounded"
                  maxLength="1"
                />
                <input
                  type="text"
                  className="m-1 text-center form-control rounded"
                  maxLength="1"
                />
                <input
                  type="text"
                  className="m-1 text-center form-control rounded"
                  maxLength="1"
                />
                <input
                  type="text"
                  class="m-1 text-center form-control rounded"
                  oninput="this.value = this.value.slice(0, 1).replace(/[^0-9]/g, '');"
                />
                <input
                  type="text"
                  className="m-1 text-center form-control rounded"
                  maxLength="1"
                />
              </div>

              <div className="mt-3 mb-5">
                <button
                  onClick={verifyClicked}
                  className="btn btn-success px-4 verify-btn"
                >
                  Verify
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
