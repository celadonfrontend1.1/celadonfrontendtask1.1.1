import React, { useState, useEffect } from "react";
import "../Home/styles.css";
import Button from "react-bootstrap/Button";
import Header from "../../Components/Header";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function Square({ value, onSquareClick }) {
  return (
    <button className="square" onClick={onSquareClick}>
      {value}
    </button>
  );
}

function Board({ xIsNext, squares, onPlay }) {
  const [bombRandomGen, setBombRandomGen] = useState(null);

  function generateRandomBomb() {
    return Math.floor(Math.random() * 4);
  }

  function handleClick(i) {
    if (calculateWinner(squares) || squares[i] || bombRandomGen === null) {
      return;
    }

    const nextSquares = squares.slice();

    if (i === bombRandomGen) {
      nextSquares[i] = "💣";
      onPlay(nextSquares);

      alert("BOMB EXPLODED...");

      const currentProfile = JSON.parse(localStorage.getItem("currentProfile"));

      if (xIsNext) {
        ++currentProfile.lose;
        --currentProfile.score;
      } else {
        ++currentProfile.win;
        ++currentProfile.score;
      }

      ++currentProfile.totalGame;

      let profilesArray = JSON.parse(localStorage.getItem("profiles")) || [];
      let currentProfileIndex = profilesArray.findIndex(
        (profile) => profile.username === currentProfile.username
      );
      profilesArray[currentProfileIndex] = currentProfile;
      localStorage.setItem("profiles", JSON.stringify(profilesArray));
      localStorage.setItem("currentProfile", JSON.stringify(currentProfile));

      return;
    }

    nextSquares[i] = "X";
    onPlay(nextSquares);

    const emptySquares = nextSquares.reduce((acc, square, index) => {
      if (!square) {
        acc.push(index);
      }
      return acc;
    }, []);

    if (emptySquares.length > 0) {
      const randomEmptyIndex =
        emptySquares[Math.floor(Math.random() * emptySquares.length)];

      setTimeout(() => {
        if (!calculateWinner(nextSquares)) {
          nextSquares[randomEmptyIndex] = "O";
          onPlay(nextSquares);
        }
      }, 750);
    }
  }

  useEffect(() => {
    if (bombRandomGen === null) {
      const randomBombIndex = generateRandomBomb();
      setBombRandomGen(randomBombIndex);
    }
  }, [bombRandomGen]);

  const currentProfile = JSON.parse(localStorage.getItem("currentProfile"));
  const winner = calculateWinner(squares);
  let status;
  if (winner) {
    status =
      winner === "X" ? "Winner: " + currentProfile.username : "Winner: AIish";

    if (winner === "X") {
      ++currentProfile.win;
      ++currentProfile.score;
    } else {
      ++currentProfile.lose;
      --currentProfile.score;
    }
    ++currentProfile.totalGame;
    let profilesArray = JSON.parse(localStorage.getItem("profiles")) || [];
    let currentProfileIndex = profilesArray.findIndex(
      (profile) => profile.username === currentProfile.username
    );
    profilesArray[currentProfileIndex] = currentProfile;
    localStorage.setItem("profiles", JSON.stringify(profilesArray));
    localStorage.setItem("currentProfile", JSON.stringify(currentProfile));
  } else {
    status = xIsNext
      ? "Next player: " + currentProfile.username
      : "Next player: AIish";
  }
  localStorage.setItem("currentProfile", JSON.stringify(currentProfile));

  return (
    <>
      <div className="status">{status}</div>
      <div className="board-row">
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
      <div className="board-row">
        <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
        <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
        <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
      </div>
      <div className="board-row">
        <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
        <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
        <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
      </div>
    </>
  );
}

function UserStats() {
  const currentProfile = JSON.parse(localStorage.getItem("currentProfile"));
  return (
    <>
      <div className="text-dark">
        <h5>Player:{currentProfile.username}</h5>
        <h5>Wins:{currentProfile.win}</h5>
        <h5>Loses:{currentProfile.lose}</h5>
        <h5>Total Games:{currentProfile.totalGame}</h5>
        <h5>Scores:{currentProfile.score}</h5>
        <h5>
          Win Rate:
          {currentProfile.totalGame > 0
            ? (currentProfile.win / currentProfile.totalGame).toFixed(2)
            : 0}
        </h5>
      </div>
    </>
  );
}

function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const xIsNext = currentMove % 2 === 0;
  const currentSquares = history[currentMove];

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  const moves = history.map((squares, move) => {
    let description;
    if (move > 0) {
      description = "Go to Move #" + move;
    } else {
      description = "Go To Game Start";
    }
    return (
      <div key={move}>
        <Button
          id="move-button-root"
          className="btn btn-dark btn-sm"
          onClick={() => jumpTo(move)}
        >
          {description}
        </Button>
      </div>
    );
  });

  return (
    <Container>
      <div className="game">
        <Row>
          <Col>
            <div id="user-stats">
              <UserStats />
            </div>
          </Col>
          <Col>
            <div className="game-board">
              <Board
                xIsNext={xIsNext}
                squares={currentSquares}
                onPlay={handlePlay}
              />
            </div>
          </Col>
          <Col id="moveButtons">
            <div className="game-info">
              <div>{moves}</div>
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  );
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export default function Home() {
  return (
    <div>
      <Header />
      <Game />
    </div>
  );
}
