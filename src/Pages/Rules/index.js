import React from "react";
import Header from "../../Components/Header";
import "./styles.css";

export const Rules = () => {
  return (
    <>
      <Header />
      <div className="rules">
        <h1>RULES</h1>
        <p>
          You start the game with an X (you). Afterwards, the computer places an
          O. The game is won by completing a trio horizontally, vertically, or
          diagonally from left to right, right to left, and diagonally. After
          each move, there is a 25% chance that the system places a bomb in one
          of the cells. If a bomb is placed, another bomb won't be placed again.
          The computer never clicks on a bomb. If you click on a bomb, you lose,
          and your score decreases. Good luck!
        </p>
      </div>
    </>
  );
};

export default Rules;
